var appRouter = function(app,edge) {
  app.get("/", function(req, res) {
    res.send("Hello World");
  });

  app.get("/account", function(req, res) {
    var accountMock = {
        "username": "nraboy",
        "password": "1234",
        "twitter": "@nraboy"
    }
    if(!req.query.username) {
        return res.send({"status": "error", "message": "missing username"});
    } else if(req.query.username != accountMock.username) {
        return res.send({"status": "error", "message": "wrong username"});
    } else {
        return res.send(accountMock);
    }
  });

  app.post("/account", function(req, res) {
    if(!req.body.username || !req.body.password || !req.body.twitter) {
        return res.send({"status": "error", "message": "missing a parameter"});
    } else {
        return res.send(req.body);
    }
  });

  app.post("/powershell", function(req, res) {
    var executePowershell  = edge.func("ps", function() {/*
      . C:\Users\tony.lea\Coding\node-powershell-api\powershell\testScript.ps1

      Create-NewFile $inputFromJS
    */});
    var testFile = "testFile.txt"
    executePowershell(testFile) 
    return res.send({Status: "Completed"})
  });

/*
  app.post("/nactool", fucntion(req, res) {

    var createNacAccount = edge.func("ps",func)

  });
*/
}

module.exports = appRouter;